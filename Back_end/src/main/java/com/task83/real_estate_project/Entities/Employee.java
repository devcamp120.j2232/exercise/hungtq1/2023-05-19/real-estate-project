package com.task83.real_estate_project.Entities;

import java.util.*;

import javax.persistence.*;

@Entity
@Table(name = "employees")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "LastName")
    private String LastName;

    @Column(name = "FirstName")
    private String FirstName;

    @Column(name = "Title")
    private String Title;

    @Column(name = "TitleOfCourtesy")
    private String TitleOfCourtesy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "BirthDate")
    private Date BirthDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "HireDate")
    private Date HireDate;

    @Column(name = "Address")
    private String Address;

    @Column(name = "City")
    private String City;

    @Column(name = "Region")
    private String Region;

    @Column(name = "PostalCode")
    private String PostalCode;

    @Column(name = "Country")
    private String Country;

    @Column(name = "HomePhone")
    private String HomePhone;

    @Column(name = "Extension")
    private String Extension;

    @Column(name = "Photo")
    private String Photo;

    @Column(name = "Notes")
    private String Notes;

    @Column(name = "ReportsTo")
    private Integer ReportsTo;

    @Column(name = "Username")
    private String Username;

    @Column(name = "Password")
    private String Password;

    @Column(name = "Email")
    private String Email;

    @Column(name = "Activated", columnDefinition = "ENUM('Y', 'N')")
    private String activated;

    @Column(name = "Profile")
    private String Profile;

    @Column(name = "UserLevel")
    private Integer UserLevel;

    public Employee() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getTitleOfCourtesy() {
        return TitleOfCourtesy;
    }

    public void setTitleOfCourtesy(String titleOfCourtesy) {
        TitleOfCourtesy = titleOfCourtesy;
    }

    public Date getBirthDate() {
        return BirthDate;
    }

    public void setBirthDate(Date birthDate) {
        BirthDate = birthDate;
    }

    public Date getHireDate() {
        return HireDate;
    }

    public void setHireDate(Date hireDate) {
        HireDate = hireDate;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String region) {
        Region = region;
    }

    public String getPostalCode() {
        return PostalCode;
    }

    public void setPostalCode(String postalCode) {
        PostalCode = postalCode;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getHomePhone() {
        return HomePhone;
    }

    public void setHomePhone(String homePhone) {
        HomePhone = homePhone;
    }

    public String getExtension() {
        return Extension;
    }

    public void setExtension(String extension) {
        Extension = extension;
    }

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String photo) {
        Photo = photo;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }

    public Integer getReportsTo() {
        return ReportsTo;
    }

    public void setReportsTo(Integer reportsTo) {
        ReportsTo = reportsTo;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getActivated() {
        return activated;
    }

    public void setActivated(String activated) {
        this.activated = activated;
    }

    public String getProfile() {
        return Profile;
    }

    public void setProfile(String profile) {
        Profile = profile;
    }

    public Integer getUserLevel() {
        return UserLevel;
    }

    public void setUserLevel(Integer userLevel) {
        UserLevel = userLevel;
    }

    

    
}
