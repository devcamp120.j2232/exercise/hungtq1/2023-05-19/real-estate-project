package com.task83.real_estate_project.Entities;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "master_layout")
public class MasterLayout {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "acreage")
    private BigDecimal acreage;
    @Column(name = "apartment_list")
    private String apartment_list;
    @Column(name = "photo")
    private String photo;

    @Column(name = "date_create", nullable = true, updatable = false)
    @CreationTimestamp
    private Date date_create;
    
    @Column(name = "date_update", nullable = true, updatable = true)
    @UpdateTimestamp
    private Date date_update;

    @ManyToOne
    private Project project;

    public MasterLayout() {
    }

    public MasterLayout(int id, String name, String description, BigDecimal acreage, String apartment_list,
            String photo, Date date_create, Date date_update, Project project) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.acreage = acreage;
        this.apartment_list = apartment_list;
        this.photo = photo;
        this.date_create = date_create;
        this.date_update = date_update;
        this.project = project;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public String getApartment_list() {
        return apartment_list;
    }

    public void setApartment_list(String apartment_list) {
        this.apartment_list = apartment_list;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Date getDate_create() {
        return date_create;
    }

    public void setDate_create(Date date_create) {
        this.date_create = date_create;
    }

    public Date getDate_update() {
        return date_update;
    }

    public void setDate_update(Date date_update) {
        this.date_update = date_update;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}
