package com.task83.real_estate_project.Entities;

import javax.persistence.*;

@Entity
@Table(name = "subscriptions")
public class Subscription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "user")
    private String user;
    @Column(name = "endpoint")
    private String endpoint;
    @Column(name = "publicKey")
    private String publicKey;
    @Column(name = "authenticationToken")
    private String authenticationToken;
    @Column(name = "contentCoding")
    private String contentCoding;
    
    public Subscription() {
    }

    public Subscription(int id, String user, String endpoint, String publicKey, String authenticationToken,
            String contentCoding) {
        this.id = id;
        this.user = user;
        this.endpoint = endpoint;
        this.publicKey = publicKey;
        this.authenticationToken = authenticationToken;
        this.contentCoding = contentCoding;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    public String getContentCoding() {
        return contentCoding;
    }

    public void setContentCoding(String contentCoding) {
        this.contentCoding = contentCoding;
    }
    
}
