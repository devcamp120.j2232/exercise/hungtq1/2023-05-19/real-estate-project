package com.task83.real_estate_project.Entities;

import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "street")
public class Street {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "_name")
  private String name;

  @Column(name = "_prefix")
  private String prefix;

  @ManyToOne
  private Province province;

  @ManyToOne
  private District district;

  @OneToMany(mappedBy = "street")
  @JsonIgnore
  private List<Project> projects;

  @OneToMany(mappedBy = "street")
  @JsonIgnore
  private List<RealEstate> realEstates;

  public Street() {
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPrefix() {
    return prefix;
  }

  public void setPrefix(String prefix) {
    this.prefix = prefix;
  }

  public Province getProvince() {
    return province;
  }

  public void setProvince(Province province) {
    this.province = province;
  }

  public District getDistrict() {
    return district;
  }

  public void setDistrict(District district) {
    this.district = district;
  }

  public List<Project> getProjects() {
    return projects;
  }

  public void setProjects(List<Project> projects) {
    this.projects = projects;
  }

  public List<RealEstate> getRealEstates() {
    return realEstates;
  }

  public void setRealEstates(List<RealEstate> realEstates) {
    this.realEstates = realEstates;
  }

  
 
}
