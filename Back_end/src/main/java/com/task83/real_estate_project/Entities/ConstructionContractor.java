package com.task83.real_estate_project.Entities;

import javax.persistence.*;

@Entity
@Table(name = "construction_contractor")
public class ConstructionContractor {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
  
    @Column(name = "name", length = 1000, nullable = false)
    private String name;
  
    @Column(name = "description", length = 5000)
    private String description;
  
    @Column(name = "projects", length = 2000)
    private String projects;
  
    @Column(name = "address")
    private Integer address;
  
    @Column(name = "phone", length = 50)
    private String phone;
  
    @Column(name = "phone2", length = 50)
    private String phone2;
  
    @Column(name = "fax", length = 50)
    private String fax;
  
    @Column(name = "email", length = 200)
    private String email;
  
    @Column(name = "website", length = 1000)
    private String website;
  
    @Column(name = "note", columnDefinition = "MEDIUMTEXT")
    private String note;

    public ConstructionContractor() {
    }

    public int getId() {
      return id;
    }

    public void setId(int id) {
      this.id = id;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getDescription() {
      return description;
    }

    public void setDescription(String description) {
      this.description = description;
    }

    public String getProjects() {
      return projects;
    }

    public void setProjects(String projects) {
      this.projects = projects;
    }

    public Integer getAddress() {
      return address;
    }

    public void setAddress(Integer address) {
      this.address = address;
    }

    public String getPhone() {
      return phone;
    }

    public void setPhone(String phone) {
      this.phone = phone;
    }

    public String getPhone2() {
      return phone2;
    }

    public void setPhone2(String phone2) {
      this.phone2 = phone2;
    }

    public String getFax() {
      return fax;
    }

    public void setFax(String fax) {
      this.fax = fax;
    }

    public String getEmail() {
      return email;
    }

    public void setEmail(String email) {
      this.email = email;
    }

    public String getWebsite() {
      return website;
    }

    public void setWebsite(String website) {
      this.website = website;
    }

    public String getNote() {
      return note;
    }

    public void setNote(String note) {
      this.note = note;
    }

  
      
   
    
}
