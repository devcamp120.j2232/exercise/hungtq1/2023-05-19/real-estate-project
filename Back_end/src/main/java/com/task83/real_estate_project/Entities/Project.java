package com.task83.real_estate_project.Entities;

import java.math.BigDecimal;
import java.util.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "project")
public class Project {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "_name", length = 200)
  private String name;

  @Column(name = "address", length = 1000)
  private String address;

  @Column(name = "slogan", columnDefinition = "MEDIUMTEXT")
  private String slogan;

  @Column(name = "description", columnDefinition = "TEXT")
  private String description;

  @Column(name = "acreage")
  private BigDecimal acreage;

  @Column(name = "construct_area")
  private BigDecimal constructArea;

  @Column(name = "num_block")
  private Short numBlock;

  @Column(name = "num_floors", length = 500)
  private String numFloors;

  @Column(name = "num_apartment")
  private Integer numApartment;

  @Column(name = "apartment_area", length = 500)
  private String apartmentArea;

  @OneToOne(targetEntity =  Investor.class)
  @JoinColumn(name = "id")
  private Investor investor;

  @OneToOne(targetEntity =  ConstructionContractor.class)
  @JoinColumn(name = "id")
  private ConstructionContractor construction_contractor;

  @OneToOne(targetEntity =  DesignUnit.class)
  @JoinColumn(name = "id")
  private DesignUnit design_unit;

  @Column(name = "utilities", length = 1000, nullable = false)
  private String utilities;

  @Column(name = "region_link", length = 1000, nullable = false)
  private String regionLink;

  @Column(name = "photo", length = 5000)
  private String photo;

  @Column(name = "_lat")
  private Double lat;

  @Column(name = "_lng")
  private Double lng;

  @OneToMany(mappedBy = "project")
  @JsonIgnore
  private List<MasterLayout> masterLayouts;

  @OneToMany(mappedBy = "project")
  @JsonIgnore
  private List<RealEstate> realEstates;

  @ManyToOne
  private Province province;

  @ManyToOne
  private District district;

  @ManyToOne
  private Ward ward;

  @ManyToOne
  private Street street;

  public Project() {
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getSlogan() {
    return slogan;
  }

  public void setSlogan(String slogan) {
    this.slogan = slogan;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public BigDecimal getAcreage() {
    return acreage;
  }

  public void setAcreage(BigDecimal acreage) {
    this.acreage = acreage;
  }

  public BigDecimal getConstructArea() {
    return constructArea;
  }

  public void setConstructArea(BigDecimal constructArea) {
    this.constructArea = constructArea;
  }

  public Short getNumBlock() {
    return numBlock;
  }

  public void setNumBlock(Short numBlock) {
    this.numBlock = numBlock;
  }

  public String getNumFloors() {
    return numFloors;
  }

  public void setNumFloors(String numFloors) {
    this.numFloors = numFloors;
  }

  public Integer getNumApartment() {
    return numApartment;
  }

  public void setNumApartment(Integer numApartment) {
    this.numApartment = numApartment;
  }

  public String getApartmentArea() {
    return apartmentArea;
  }

  public void setApartmentArea(String apartmentArea) {
    this.apartmentArea = apartmentArea;
  }

  public Investor getInvestor() {
    return investor;
  }

  public void setInvestor(Investor investor) {
    this.investor = investor;
  }

  public ConstructionContractor getConstruction_contractor() {
    return construction_contractor;
  }

  public void setConstruction_contractor(ConstructionContractor construction_contractor) {
    this.construction_contractor = construction_contractor;
  }

  public DesignUnit getDesign_unit() {
    return design_unit;
  }

  public void setDesign_unit(DesignUnit design_unit) {
    this.design_unit = design_unit;
  }

  public String getUtilities() {
    return utilities;
  }

  public void setUtilities(String utilities) {
    this.utilities = utilities;
  }

  public String getRegionLink() {
    return regionLink;
  }

  public void setRegionLink(String regionLink) {
    this.regionLink = regionLink;
  }

  public String getPhoto() {
    return photo;
  }

  public void setPhoto(String photo) {
    this.photo = photo;
  }

  public Double getLat() {
    return lat;
  }

  public void setLat(Double lat) {
    this.lat = lat;
  }

  public Double getLng() {
    return lng;
  }

  public void setLng(Double lng) {
    this.lng = lng;
  }

  public List<MasterLayout> getMasterLayouts() {
    return masterLayouts;
  }

  public void setMasterLayouts(List<MasterLayout> masterLayouts) {
    this.masterLayouts = masterLayouts;
  }

  public List<RealEstate> getRealEstates() {
    return realEstates;
  }

  public void setRealEstates(List<RealEstate> realEstates) {
    this.realEstates = realEstates;
  }

  public Province getProvince() {
    return province;
  }

  public void setProvince(Province province) {
    this.province = province;
  }

  public District getDistrict() {
    return district;
  }

  public void setDistrict(District district) {
    this.district = district;
  }

  public Ward getWard() {
    return ward;
  }

  public void setWard(Ward ward) {
    this.ward = ward;
  }

  public Street getStreet() {
    return street;
  }

  public void setStreet(Street street) {
    this.street = street;
  }

  
  
  
  
} 