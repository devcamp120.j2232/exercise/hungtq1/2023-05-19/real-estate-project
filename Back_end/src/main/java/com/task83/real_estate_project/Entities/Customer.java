package com.task83.real_estate_project.Entities;

import java.util.*;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "customers")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "contact_name")
    private String contact_name;
    @Column(name = "contact_title")
    private String contact_title;
    @Column(name = "address")
    private String address;
    @Column(name = "mobile")
    private String mobile;
    @Column(name = "email")
    private String email;
    @Column(name = "note")
    private String note;

    //@Temporal(TemporalType.DATE)
    @Column(name = "create_date", nullable = true, updatable = false)
    //@CreatedDate
    //@JsonFormat(pattern = "dd-MM-yyyy")
    @CreationTimestamp
    private Date create_date;
    
    //@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_date", nullable = true, updatable = true)
    //@LastModifiedDate
    //@JsonFormat(pattern = "dd-MM-yyyy")
    @UpdateTimestamp
    private Date update_date;
    
    @Column(name = "create_by")
    private String create_by;
    @Column(name = "update_by")
    private String update_by;

    @OneToMany(mappedBy = "customer")
    @JsonIgnore
    private List<RealEstate> realEstates;

    public Customer() {
    }

    public Customer(int id, String contact_name, String contact_title, String address, String mobile, String email,
            String note, Date create_date, Date update_date, String create_by, String update_by,
            List<RealEstate> realEstates) {
        this.id = id;
        this.contact_name = contact_name;
        this.contact_title = contact_title;
        this.address = address;
        this.mobile = mobile;
        this.email = email;
        this.note = note;
        this.create_date = create_date;
        this.update_date = update_date;
        this.create_by = create_by;
        this.update_by = update_by;
        this.realEstates = realEstates;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public String getContact_title() {
        return contact_title;
    }

    public void setContact_title(String contact_title) {
        this.contact_title = contact_title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public String getCreate_by() {
        return create_by;
    }

    public void setCreate_by(String create_by) {
        this.create_by = create_by;
    }

    public String getUpdate_by() {
        return update_by;
    }

    public void setUpdate_by(String update_by) {
        this.update_by = update_by;
    }

    public List<RealEstate> getRealEstates() {
        return realEstates;
    }

    public void setRealEstates(List<RealEstate> realEstates) {
        this.realEstates = realEstates;
    }
  
    
    
    
}
