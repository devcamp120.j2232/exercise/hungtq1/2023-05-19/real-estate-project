package com.task83.real_estate_project.Entities;

import java.sql.Date;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "realestate")
public class RealEstate {

    @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "title", length = 2000)
  private String title;

  @Column(name = "type")
  private Integer type;

  @Column(name = "request")
  private Integer request;

  @Column(name = "address", nullable = false, length = 2000)
  private String address;

  @Column(name = "price")
  private Long price;

  @Column(name = "price_min")
  private Long priceMin;

  @Column(name = "price_time")
  private Integer priceTime;

  @Column(name = "date_create", nullable = false)
  @CreationTimestamp
  private Date dateCreate;

  @Column(name = "acreage")
  private Double acreage;

  @Column(name = "direction")
  private Integer direction;

  @Column(name = "total_floors")
  private Integer totalFloors;

  @Column(name = "number_floors")
  private Integer numberFloors;

  @Column(name = "bath")
  private Integer bath;

  @Column(name = "apart_code", length = 10)
  private String apartCode;

  @Column(name = "wall_area")
  private Double wallArea;

  @Column(name = "bedroom")
  private Integer bedroom;

  @Column(name = "balcony")
  private Integer balcony;

  @Column(name = "landscape_view", length = 255)
  private String landscapeView;

  @Column(name = "apart_loca")
  private Integer apartLoca;

  @Column(name = "apart_type")
  private Integer apartType;

  @Column(name = "furniture_type")
  private Integer furnitureType;

  @Column(name = "price_rent")
  private Integer priceRent;

  @Column(name = "return_rate")
  private Double returnRate;

  @Column(name = "legal_doc")
  private Integer legalDoc;

  @Column(name = "description", length = 2000)
  private String description;

  @Column(name = "width_y")
  private Integer widthY;

  @Column(name = "long_x")
  private Integer longX;

  @Column(name = "street_house")
  private Integer streetHouse;

  @Column(name = "FSBO")
  private Integer fsbo;

  @Column(name = "view_num")
  private Integer viewNum;

  @Column(name = "create_by")
  private Integer createBy;

  @Column(name = "update_by")
  private Integer updateBy;

  @Column(name = "shape", length = 200)
  private String shape;

  @Column(name = "distance2facade")
  private Integer distance2facade;

  @Column(name = "adjacent_facade_num")
  private Integer adjacentFacadeNum;

  @Column(name = "adjacent_road", length = 200)
  private String adjacentRoad;

  @Column(name = "alley_min_width")
  private Integer alleyMinWidth;

  @Column(name = "adjacent_alley_min_width")
  private Integer adjacentAlleyMinWidth;

  @Column(name = "factor")
  private Integer factor;

  @Column(name = "structure")
  private String structure;

  @Column(name = "DTSXD")
  private Integer DTSXD;

  @Column(name = "CLCL")
  private Integer CLCL;

  @Column(name = "CTXD_price")
  private Integer CTXDPrice;

  @Column(name = "CTXD_value")
  private Integer CTXDValue;

  @Column(name = "photo")
  private String photo;

  @Column(name = "_lat")
  private Double lat;

  @Column(name = "_lng")
  private Double lng;

  @ManyToOne
  private Province province;

  @ManyToOne
  private District district;

  @ManyToOne
  private Ward ward;

  @ManyToOne
  private Street street;

  @ManyToOne
  private Project project;

  @ManyToOne
  private Customer customer;

 
  public RealEstate() {

  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public Integer getRequest() {
    return request;
  }

  public void setRequest(Integer request) {
    this.request = request;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Long getPrice() {
    return price;
  }

  public void setPrice(Long price) {
    this.price = price;
  }

  public Long getPriceMin() {
    return priceMin;
  }

  public void setPriceMin(Long priceMin) {
    this.priceMin = priceMin;
  }

  public Integer getPriceTime() {
    return priceTime;
  }

  public void setPriceTime(Integer priceTime) {
    this.priceTime = priceTime;
  }

  public Date getDateCreate() {
    return dateCreate;
  }

  public void setDateCreate(Date dateCreate) {
    this.dateCreate = dateCreate;
  }

  public Double getAcreage() {
    return acreage;
  }

  public void setAcreage(Double acreage) {
    this.acreage = acreage;
  }

  public Integer getDirection() {
    return direction;
  }

  public void setDirection(Integer direction) {
    this.direction = direction;
  }

  public Integer getTotalFloors() {
    return totalFloors;
  }

  public void setTotalFloors(Integer totalFloors) {
    this.totalFloors = totalFloors;
  }

  public Integer getNumberFloors() {
    return numberFloors;
  }

  public void setNumberFloors(Integer numberFloors) {
    this.numberFloors = numberFloors;
  }

  public Integer getBath() {
    return bath;
  }

  public void setBath(Integer bath) {
    this.bath = bath;
  }

  public String getApartCode() {
    return apartCode;
  }

  public void setApartCode(String apartCode) {
    this.apartCode = apartCode;
  }

  public Double getWallArea() {
    return wallArea;
  }

  public void setWallArea(Double wallArea) {
    this.wallArea = wallArea;
  }

  public Integer getBedroom() {
    return bedroom;
  }

  public void setBedroom(Integer bedroom) {
    this.bedroom = bedroom;
  }

  public Integer getBalcony() {
    return balcony;
  }

  public void setBalcony(Integer balcony) {
    this.balcony = balcony;
  }

  public String getLandscapeView() {
    return landscapeView;
  }

  public void setLandscapeView(String landscapeView) {
    this.landscapeView = landscapeView;
  }

  public Integer getApartLoca() {
    return apartLoca;
  }

  public void setApartLoca(Integer apartLoca) {
    this.apartLoca = apartLoca;
  }

  public Integer getApartType() {
    return apartType;
  }

  public void setApartType(Integer apartType) {
    this.apartType = apartType;
  }

  public Integer getFurnitureType() {
    return furnitureType;
  }

  public void setFurnitureType(Integer furnitureType) {
    this.furnitureType = furnitureType;
  }

  public Integer getPriceRent() {
    return priceRent;
  }

  public void setPriceRent(Integer priceRent) {
    this.priceRent = priceRent;
  }

  public Double getReturnRate() {
    return returnRate;
  }

  public void setReturnRate(Double returnRate) {
    this.returnRate = returnRate;
  }

  public Integer getLegalDoc() {
    return legalDoc;
  }

  public void setLegalDoc(Integer legalDoc) {
    this.legalDoc = legalDoc;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Integer getWidthY() {
    return widthY;
  }

  public void setWidthY(Integer widthY) {
    this.widthY = widthY;
  }

  public Integer getLongX() {
    return longX;
  }

  public void setLongX(Integer longX) {
    this.longX = longX;
  }

  public Integer getStreetHouse() {
    return streetHouse;
  }

  public void setStreetHouse(Integer streetHouse) {
    this.streetHouse = streetHouse;
  }

  public Integer getFsbo() {
    return fsbo;
  }

  public void setFsbo(Integer fsbo) {
    this.fsbo = fsbo;
  }

  public Integer getViewNum() {
    return viewNum;
  }

  public void setViewNum(Integer viewNum) {
    this.viewNum = viewNum;
  }

  public Integer getCreateBy() {
    return createBy;
  }

  public void setCreateBy(Integer createBy) {
    this.createBy = createBy;
  }

  public Integer getUpdateBy() {
    return updateBy;
  }

  public void setUpdateBy(Integer updateBy) {
    this.updateBy = updateBy;
  }

  public String getShape() {
    return shape;
  }

  public void setShape(String shape) {
    this.shape = shape;
  }

  public Integer getDistance2facade() {
    return distance2facade;
  }

  public void setDistance2facade(Integer distance2facade) {
    this.distance2facade = distance2facade;
  }

  public Integer getAdjacentFacadeNum() {
    return adjacentFacadeNum;
  }

  public void setAdjacentFacadeNum(Integer adjacentFacadeNum) {
    this.adjacentFacadeNum = adjacentFacadeNum;
  }

  public String getAdjacentRoad() {
    return adjacentRoad;
  }

  public void setAdjacentRoad(String adjacentRoad) {
    this.adjacentRoad = adjacentRoad;
  }

  public Integer getAlleyMinWidth() {
    return alleyMinWidth;
  }

  public void setAlleyMinWidth(Integer alleyMinWidth) {
    this.alleyMinWidth = alleyMinWidth;
  }

  public Integer getAdjacentAlleyMinWidth() {
    return adjacentAlleyMinWidth;
  }

  public void setAdjacentAlleyMinWidth(Integer adjacentAlleyMinWidth) {
    this.adjacentAlleyMinWidth = adjacentAlleyMinWidth;
  }

  public Integer getFactor() {
    return factor;
  }

  public void setFactor(Integer factor) {
    this.factor = factor;
  }

  public String getStructure() {
    return structure;
  }

  public void setStructure(String structure) {
    this.structure = structure;
  }

  public Integer getDTSXD() {
    return DTSXD;
  }

  public void setDTSXD(Integer dTSXD) {
    DTSXD = dTSXD;
  }

  public Integer getCLCL() {
    return CLCL;
  }

  public void setCLCL(Integer cLCL) {
    CLCL = cLCL;
  }

  public Integer getCTXDPrice() {
    return CTXDPrice;
  }

  public void setCTXDPrice(Integer cTXDPrice) {
    CTXDPrice = cTXDPrice;
  }

  public Integer getCTXDValue() {
    return CTXDValue;
  }

  public void setCTXDValue(Integer cTXDValue) {
    CTXDValue = cTXDValue;
  }

  public String getPhoto() {
    return photo;
  }

  public void setPhoto(String photo) {
    this.photo = photo;
  }

  public Double getLat() {
    return lat;
  }

  public void setLat(Double lat) {
    this.lat = lat;
  }

  public Double getLng() {
    return lng;
  }

  public void setLng(Double lng) {
    this.lng = lng;
  }

  public Province getProvince() {
    return province;
  }

  public void setProvince(Province province) {
    this.province = province;
  }

  public District getDistrict() {
    return district;
  }

  public void setDistrict(District district) {
    this.district = district;
  }

  public Ward getWard() {
    return ward;
  }

  public void setWard(Ward ward) {
    this.ward = ward;
  }

  public Street getStreet() {
    return street;
  }

  public void setStreet(Street street) {
    this.street = street;
  }

  public Project getProject() {
    return project;
  }

  public void setProject(Project project) {
    this.project = project;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }
    

}
