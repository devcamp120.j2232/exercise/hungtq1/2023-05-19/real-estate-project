package com.task83.real_estate_project.Entities;

import javax.persistence.*;

@Entity
@Table(name = "address_map")
public class AddressMap {
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "address", length = 5000, nullable = false)
  private String address;

  @Column(name = "_lat", nullable = false)
  private Double latitude;

  @Column(name = "_lng", nullable = false)
  private Double longitude;

  public AddressMap() {
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

}
