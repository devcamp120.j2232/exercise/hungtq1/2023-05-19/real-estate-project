package com.task83.real_estate_project.Entities;

import java.util.*;


import javax.persistence.*;

import com.fasterxml.jackson.annotation.*;

@Entity
@Table(name = "province")
public class Province {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "_name")
    private String name;

    @Column(name = "_code")
    private String code;

    @OneToMany(mappedBy = "province")
    @JsonIgnore
    private List<District> districts;

    @OneToMany(mappedBy = "province")
    @JsonIgnore
    private List<Project> projects;

    @OneToMany(mappedBy = "province")
    @JsonIgnore
    private List<RealEstate> realeStates;

    @OneToMany(mappedBy = "province")
    @JsonIgnore
    private List<Street> streets;

    @OneToMany(mappedBy = "province")
    @JsonIgnore
    private List<Ward> wards;

    public Province() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<District> getDistricts() {
        return districts;
    }

    public void setDistricts(List<District> districts) {
        this.districts = districts;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<RealEstate> getRealeStates() {
        return realeStates;
    }

    public void setRealeStates(List<RealEstate> realeStates) {
        this.realeStates = realeStates;
    }

    public List<Street> getStreets() {
        return streets;
    }

    public void setStreets(List<Street> streets) {
        this.streets = streets;
    }

    public List<Ward> getWards() {
        return wards;
    }

    public void setWards(List<Ward> wards) {
        this.wards = wards;
    }
    

   
   
}
