package com.task83.real_estate_project.Entities;

import java.util.*;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "district")
public class District {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "_name")
    private String name;

    @Column(name = "_prefix")
    private String prefix;

    @ManyToOne
    private Province province;

    @OneToMany(mappedBy = "district")
    @JsonIgnore
    private List<Project> projects;

    @OneToMany(mappedBy = "district")
    @JsonIgnore
    private List<RealEstate> realeStates;

    @OneToMany(mappedBy = "district")
    @JsonIgnore
    private List<Street> streets;

    @OneToMany(mappedBy = "district")
    @JsonIgnore
    private List<Ward> wards;

    public District() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<RealEstate> getRealeStates() {
        return realeStates;
    }

    public void setRealeStates(List<RealEstate> realeStates) {
        this.realeStates = realeStates;
    }

    public List<Street> getStreets() {
        return streets;
    }

    public void setStreets(List<Street> streets) {
        this.streets = streets;
    }

    public List<Ward> getWards() {
        return wards;
    }

    public void setWards(List<Ward> wards) {
        this.wards = wards;
        
    }
}
