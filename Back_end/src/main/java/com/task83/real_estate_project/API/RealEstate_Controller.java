package com.task83.real_estate_project.API;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.task83.real_estate_project.Entities.Customer;
import com.task83.real_estate_project.Entities.District;
import com.task83.real_estate_project.Entities.Project;
import com.task83.real_estate_project.Entities.Province;
import com.task83.real_estate_project.Entities.RealEstate;
import com.task83.real_estate_project.Entities.Street;
import com.task83.real_estate_project.Entities.Ward;
import com.task83.real_estate_project.Repository.Customer_Repository;
import com.task83.real_estate_project.Repository.District_Repository;
import com.task83.real_estate_project.Repository.Project_Repository;
import com.task83.real_estate_project.Repository.Province_Repository;
import com.task83.real_estate_project.Repository.RealEstate_Repository;
import com.task83.real_estate_project.Repository.Street_Repository;
import com.task83.real_estate_project.Repository.Ward_Repository;
import com.task83.real_estate_project.Service.RealEstate_Service;

@RestController
@CrossOrigin
@RequestMapping("/real_estate")
public class RealEstate_Controller {
    
    @Autowired
    RealEstate_Repository realEstate_Repository;

    @Autowired
    RealEstate_Service realEstate_Service;

    @Autowired
    Province_Repository province_Repository;

    @Autowired
    District_Repository district_Repository;

    @Autowired
    Ward_Repository ward_Repository;

    @Autowired
    Street_Repository street_Repository;

    @Autowired
    Customer_Repository customer_Repository;

    @Autowired
    Project_Repository project_Repository;


    @GetMapping("/all")
  public ResponseEntity<List<RealEstate>> getAllRealeState() {
    try {
      return new ResponseEntity<>(realEstate_Service.getListAllRealEstate(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("/create/{provinceId}/{districtId}/{wardId}/{streetId}/{projectId}/{customerId}")
    public ResponseEntity<RealEstate> createNewRealEstate(
      @PathVariable(value = "provinceId") int provinceId,
      @PathVariable(value = "districtId") int districtId,
      @PathVariable(value = "wardId") int wardId,
      @PathVariable(value = "streetId") int streetId, 
      @PathVariable(value = "projectId") int projectId,
      @PathVariable(value = "customerId") int customerId,
      @RequestBody RealEstate pRealEstate) {

        Project project = project_Repository.findById(projectId);
        Province province = province_Repository.findById(provinceId);
        District district = district_Repository.findById(districtId);
        Ward ward = ward_Repository.findById(wardId);
        Street street = street_Repository.findById(streetId);
        Customer customer = customer_Repository.findById(customerId);
      
        try {
          RealEstate newRealEstate = new RealEstate();
            newRealEstate.setTitle(pRealEstate.getTitle());
            newRealEstate.setType(pRealEstate.getType());
            newRealEstate.setRequest(pRealEstate.getRequest());

            newRealEstate.setProvince(province);
            newRealEstate.setDistrict(district);
            newRealEstate.setWard(ward);
            newRealEstate.setStreet(street);
            newRealEstate.setProject(project);
            
            newRealEstate.setAddress(pRealEstate.getAddress());

            newRealEstate.setCustomer(customer);
            
            newRealEstate.setPrice(pRealEstate.getPrice());
            newRealEstate.setPriceMin(pRealEstate.getPriceMin());
            newRealEstate.setPriceTime(pRealEstate.getPriceTime());
            newRealEstate.setDateCreate(pRealEstate.getDateCreate());
            newRealEstate.setAcreage(pRealEstate.getAcreage());
            newRealEstate.setDirection(pRealEstate.getDirection());
            newRealEstate.setTotalFloors(pRealEstate.getTotalFloors());
            newRealEstate.setNumberFloors(pRealEstate.getNumberFloors());
            newRealEstate.setBath(pRealEstate.getBath());
            newRealEstate.setApartCode(pRealEstate.getApartCode());
            newRealEstate.setWallArea(pRealEstate.getWallArea());
            newRealEstate.setBedroom(pRealEstate.getBedroom());
            newRealEstate.setBalcony(pRealEstate.getBalcony());
            newRealEstate.setLandscapeView(pRealEstate.getLandscapeView());
            newRealEstate.setApartLoca(pRealEstate.getApartLoca());
            newRealEstate.setApartType(pRealEstate.getApartType());
            newRealEstate.setFurnitureType(pRealEstate.getFurnitureType());
            newRealEstate.setPriceRent(pRealEstate.getPriceRent());
            newRealEstate.setReturnRate(pRealEstate.getReturnRate());
            newRealEstate.setLegalDoc(pRealEstate.getLegalDoc());
            newRealEstate.setDescription(pRealEstate.getDescription());
            newRealEstate.setWidthY(pRealEstate.getWidthY());
            newRealEstate.setLongX(pRealEstate.getLongX());
            newRealEstate.setStreetHouse(pRealEstate.getStreetHouse());
            newRealEstate.setFsbo(pRealEstate.getFsbo());
            newRealEstate.setViewNum(pRealEstate.getViewNum());
            newRealEstate.setCreateBy(pRealEstate.getCreateBy());
            newRealEstate.setUpdateBy(pRealEstate.getUpdateBy());
            newRealEstate.setShape(pRealEstate.getShape());
            newRealEstate.setDistance2facade(pRealEstate.getDistance2facade());
            newRealEstate.setAdjacentFacadeNum(pRealEstate.getAdjacentFacadeNum());
            newRealEstate.setAdjacentRoad(pRealEstate.getAdjacentRoad());
            newRealEstate.setAlleyMinWidth(pRealEstate.getAlleyMinWidth());
            newRealEstate.setAdjacentAlleyMinWidth(pRealEstate.getAdjacentAlleyMinWidth());
            newRealEstate.setFactor(pRealEstate.getFactor());
            newRealEstate.setStructure(pRealEstate.getStructure());
            newRealEstate.setDTSXD(pRealEstate.getDTSXD());
            newRealEstate.setCLCL(pRealEstate.getCLCL());
            newRealEstate.setCTXDPrice(pRealEstate.getCTXDPrice());
            newRealEstate.setCTXDValue(pRealEstate.getCTXDValue());
            newRealEstate.setPhoto(pRealEstate.getPhoto());
            newRealEstate.setLat(pRealEstate.getLat());
            newRealEstate.setLng(pRealEstate.getLng());
            return new ResponseEntity<>(realEstate_Repository.save(newRealEstate), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
    @PutMapping("/update/{id}/{provinceId}/{districtId}/{wardId}/{streetId}/{projectId}/{customerId}")
    public ResponseEntity<RealEstate> updateRealEstate(
      @PathVariable("id") int id, 
      @PathVariable(value = "provinceId") int provinceId,
      @PathVariable(value = "districtId") int districtId,
      @PathVariable(value = "wardId") int wardId,
      @PathVariable(value = "streetId") int streetId, 
      @PathVariable(value = "projectId") int projectId,
      @PathVariable(value = "customerId") int customerId,
      @RequestBody RealEstate pRealEstate){

        RealEstate realEstate = realEstate_Repository.findById(id);
        Project project = project_Repository.findById(projectId);
        Province province = province_Repository.findById(provinceId);
        District district = district_Repository.findById(districtId);
        Ward ward = ward_Repository.findById(wardId);
        Street street = street_Repository.findById(streetId);
        Customer customer = customer_Repository.findById(customerId);

        try {
            if(realEstate != null){
              realEstate.setTitle(pRealEstate.getTitle());
              realEstate.setType(pRealEstate.getType());
              realEstate.setRequest(pRealEstate.getRequest());
  
              realEstate.setProvince(province);
              realEstate.setDistrict(district);
              realEstate.setWard(ward);
              realEstate.setStreet(street);
              realEstate.setProject(project);
              
              realEstate.setAddress(pRealEstate.getAddress());
  
              realEstate.setCustomer(customer);
              
              realEstate.setPrice(pRealEstate.getPrice());
              realEstate.setPriceMin(pRealEstate.getPriceMin());
              realEstate.setPriceTime(pRealEstate.getPriceTime());
              realEstate.setDateCreate(pRealEstate.getDateCreate());
              realEstate.setAcreage(pRealEstate.getAcreage());
              realEstate.setDirection(pRealEstate.getDirection());
              realEstate.setTotalFloors(pRealEstate.getTotalFloors());
              realEstate.setNumberFloors(pRealEstate.getNumberFloors());
              realEstate.setBath(pRealEstate.getBath());
              realEstate.setApartCode(pRealEstate.getApartCode());
              realEstate.setWallArea(pRealEstate.getWallArea());
              realEstate.setBedroom(pRealEstate.getBedroom());
              realEstate.setBalcony(pRealEstate.getBalcony());
              realEstate.setLandscapeView(pRealEstate.getLandscapeView());
              realEstate.setApartLoca(pRealEstate.getApartLoca());
              realEstate.setApartType(pRealEstate.getApartType());
              realEstate.setFurnitureType(pRealEstate.getFurnitureType());
              realEstate.setPriceRent(pRealEstate.getPriceRent());
              realEstate.setReturnRate(pRealEstate.getReturnRate());
              realEstate.setLegalDoc(pRealEstate.getLegalDoc());
              realEstate.setDescription(pRealEstate.getDescription());
              realEstate.setWidthY(pRealEstate.getWidthY());
              realEstate.setLongX(pRealEstate.getLongX());
              realEstate.setStreetHouse(pRealEstate.getStreetHouse());
              realEstate.setFsbo(pRealEstate.getFsbo());
              realEstate.setViewNum(pRealEstate.getViewNum());
              realEstate.setCreateBy(pRealEstate.getCreateBy());
              realEstate.setUpdateBy(pRealEstate.getUpdateBy());
              realEstate.setShape(pRealEstate.getShape());
              realEstate.setDistance2facade(pRealEstate.getDistance2facade());
              realEstate.setAdjacentFacadeNum(pRealEstate.getAdjacentFacadeNum());
              realEstate.setAdjacentRoad(pRealEstate.getAdjacentRoad());
              realEstate.setAlleyMinWidth(pRealEstate.getAlleyMinWidth());
              realEstate.setAdjacentAlleyMinWidth(pRealEstate.getAdjacentAlleyMinWidth());
              realEstate.setFactor(pRealEstate.getFactor());
              realEstate.setStructure(pRealEstate.getStructure());
              realEstate.setDTSXD(pRealEstate.getDTSXD());
              realEstate.setCLCL(pRealEstate.getCLCL());
              realEstate.setCTXDPrice(pRealEstate.getCTXDPrice());
              realEstate.setCTXDValue(pRealEstate.getCTXDValue());
              realEstate.setPhoto(pRealEstate.getPhoto());
              realEstate.setLat(pRealEstate.getLat());
              realEstate.setLng(pRealEstate.getLng());
              return new ResponseEntity<>(realEstate_Repository.save(realEstate),HttpStatus.OK);
            }else{
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<RealEstate> deleteProject(@PathVariable("id") int id){
        try {
          RealEstate realEstate = realEstate_Repository.findById(id);
          realEstate_Repository.delete(realEstate);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
