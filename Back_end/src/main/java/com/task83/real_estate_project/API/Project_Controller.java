package com.task83.real_estate_project.API;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.task83.real_estate_project.Entities.Project;
import com.task83.real_estate_project.Repository.Project_Repository;
import com.task83.real_estate_project.Service.Project_Service;

@RestController
@CrossOrigin
@RequestMapping("/project")
public class Project_Controller {
    
    @Autowired
    Project_Repository project_Repository;

    @Autowired
    Project_Service project_Service;

    @GetMapping("/all")
    public ResponseEntity<List<Project>> getAllProject() {
        try {
            return new ResponseEntity<>(project_Service.getListAllProjects(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Project> createNewAddress(@RequestBody Project pProject) {
        try {
            Project newProject = new Project();
            newProject.setName(pProject.getName());
            newProject.setProvince(pProject.getProvince());
            newProject.setDistrict(pProject.getDistrict());
            newProject.setWard(pProject.getWard());
            newProject.setStreet(pProject.getStreet());
            newProject.setAddress(pProject.getAddress());
            newProject.setSlogan(pProject.getSlogan());
            newProject.setDescription(pProject.getDescription());
            newProject.setAcreage(pProject.getAcreage());
            newProject.setConstructArea(pProject.getConstructArea());
            newProject.setNumBlock(pProject.getNumBlock());
            newProject.setNumFloors(pProject.getNumFloors());
            newProject.setNumApartment(pProject.getNumApartment());
            newProject.setApartmentArea(pProject.getApartmentArea());
            newProject.setInvestor(pProject.getInvestor());
            newProject.setConstruction_contractor(pProject.getConstruction_contractor());
            newProject.setDesign_unit(pProject.getDesign_unit());
            newProject.setUtilities(pProject.getUtilities());
            newProject.setRegionLink(pProject.getRegionLink());
            newProject.setPhoto(pProject.getPhoto());
            newProject.setLat(pProject.getLat());
            newProject.setLng(pProject.getLng());
            return new ResponseEntity<>(project_Repository.save(newProject), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
    @PutMapping("/update/{id}")
    public ResponseEntity<Project> updateProject(@PathVariable("id") int id, @RequestBody Project pProject){
        try {
            Project updatedProject = project_Repository.findById(id);
            if(updatedProject != null){
                updatedProject.setName(pProject.getName());
                updatedProject.setProvince(pProject.getProvince());
                updatedProject.setDistrict(pProject.getDistrict());
                updatedProject.setWard(pProject.getWard());
                updatedProject.setStreet(pProject.getStreet());
                updatedProject.setAddress(pProject.getAddress());
                updatedProject.setSlogan(pProject.getSlogan());
                updatedProject.setDescription(pProject.getDescription());
                updatedProject.setAcreage(pProject.getAcreage());
                updatedProject.setConstructArea(pProject.getConstructArea());
                updatedProject.setNumBlock(pProject.getNumBlock());
                updatedProject.setNumFloors(pProject.getNumFloors());
                updatedProject.setNumApartment(pProject.getNumApartment());
                updatedProject.setApartmentArea(pProject.getApartmentArea());
                updatedProject.setInvestor(pProject.getInvestor());
                updatedProject.setConstruction_contractor(pProject.getConstruction_contractor());
                updatedProject.setDesign_unit(pProject.getDesign_unit());
                updatedProject.setUtilities(pProject.getUtilities());
                updatedProject.setRegionLink(pProject.getRegionLink());
                updatedProject.setPhoto(pProject.getPhoto());
                updatedProject.setLat(pProject.getLat());
                updatedProject.setLng(pProject.getLng());
                return new ResponseEntity<>(project_Repository.save(updatedProject),HttpStatus.OK);
            }else{
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Project> deleteProject(@PathVariable("id") int id){
        try {
            Project project = project_Repository.findById(id);
            project_Repository.delete(project);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
