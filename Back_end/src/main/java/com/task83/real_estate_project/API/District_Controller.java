package com.task83.real_estate_project.API;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.task83.real_estate_project.Entities.District;
import com.task83.real_estate_project.Entities.Province;
import com.task83.real_estate_project.Repository.District_Repository;
import com.task83.real_estate_project.Repository.Province_Repository;
import com.task83.real_estate_project.Service.District_Service;

@RestController
@CrossOrigin
@RequestMapping("/district")
public class District_Controller {
    
    @Autowired
    District_Repository district_Repository;

    @Autowired
    District_Service district_Service;

    @Autowired
    Province_Repository province_Repository;

    @GetMapping("/all")
    public ResponseEntity<List<District>> getAllDistrict() {
        try {
            return new ResponseEntity<>(district_Service.getListDistrict(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping("/create/{id}")
    public ResponseEntity<Object> createDistrict(@PathVariable(name = "id") int id, @RequestBody District pDistrict){
        Province province = province_Repository.findById(id);
        try {
            if(province != null){
            District newDistrict = new District();
            newDistrict.setName(pDistrict.getName());
            newDistrict.setPrefix(pDistrict.getPrefix());
            newDistrict.setProvince(province);
            return new ResponseEntity<>(district_Repository.save(newDistrict), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }                
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{districtId}/{provinceId}")
    public ResponseEntity<Object> updateDistrict(@PathVariable(value = "districtId") int districtId, @PathVariable(value = "provinceId") int provinceId,  @RequestBody District pDistrict){
        try {
            District district = district_Repository.findById(districtId);
            Province province = province_Repository.findById(provinceId);
            if(district != null){
                district.setName(pDistrict.getName());
                district.setPrefix(pDistrict.getPrefix());
                district.setProvince(province);
                return new ResponseEntity<>(district_Repository.save(district), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 
    
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<District> deleteDistrict(@PathVariable("id") int id){
        try {
            District district = district_Repository.findById(id);
            district_Repository.delete(district);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);          
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
