package com.task83.real_estate_project.API;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.task83.real_estate_project.Entities.MasterLayout;
import com.task83.real_estate_project.Entities.Project;
import com.task83.real_estate_project.Repository.MasterLayout_Repository;
import com.task83.real_estate_project.Repository.Project_Repository;
import com.task83.real_estate_project.Service.MasterLayout_Service;

@RestController
@CrossOrigin
@RequestMapping("/master_layout")
public class MasterLayout_Controller {
    
    @Autowired
    MasterLayout_Repository masterLayout_Repository;

    @Autowired
    MasterLayout_Service masterLayout_Service;

    @Autowired
    Project_Repository project_Repository;

    @GetMapping("/all")
    public ResponseEntity<List<MasterLayout>> getAllMasterLayout() {
        try {
            return new ResponseEntity<>(masterLayout_Service.getListAllMasterLayout(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create/{id}")
    public ResponseEntity<MasterLayout> createNewAddress(@PathVariable(name = "id") int id, @RequestBody MasterLayout pMasterLayout) {
        Project project = project_Repository.findById(id);
        try {
            if(project !=null){
               MasterLayout newLayout = new MasterLayout();
                newLayout.setName(pMasterLayout.getName());
                newLayout.setDescription(pMasterLayout.getDescription());
                newLayout.setProject(project);
                newLayout.setApartment_list(pMasterLayout.getApartment_list());
                newLayout.setPhoto(pMasterLayout.getPhoto());
                newLayout.setDate_create(new Date(0));
                newLayout.setDate_update(null);
                newLayout.setPhoto(pMasterLayout.getPhoto());
                newLayout.setAcreage(pMasterLayout.getAcreage());
                return new ResponseEntity<>(masterLayout_Repository.save(newLayout), HttpStatus.CREATED);
            }else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }                
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
    @PutMapping("/update/{layoutId}/{projectId}")
    public ResponseEntity<Object> updateLayout(@PathVariable(value = "layoutId") int layoutId, @PathVariable(value = "projectId") int projectId,  @RequestBody MasterLayout pLayout){
        try {
            MasterLayout layout = masterLayout_Repository.findById(layoutId);
            Project project = project_Repository.findById(projectId);
            if(layout != null){
                layout.setName(pLayout.getName());
                layout.setDescription(pLayout.getDescription());
                layout.setProject(project);
                layout.setAcreage(pLayout.getAcreage());
                layout.setApartment_list(pLayout.getApartment_list());
                layout.setPhoto(pLayout.getPhoto());
                layout.setDate_create(null);
                layout.setDate_update(new Date(projectId));
                return new ResponseEntity<>(masterLayout_Repository.save(layout), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<MasterLayout> deleteLayout(@PathVariable("id") int id){
        try {
            MasterLayout layout = masterLayout_Repository.findById(id);
            masterLayout_Repository.delete(layout);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
