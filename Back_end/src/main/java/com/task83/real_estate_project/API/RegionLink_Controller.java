package com.task83.real_estate_project.API;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.task83.real_estate_project.Entities.RegionLink;
import com.task83.real_estate_project.Repository.RegionLink_Repository;
import com.task83.real_estate_project.Service.RegionLink_Service;

@RestController
@CrossOrigin
@RequestMapping("/region_link")
public class RegionLink_Controller {
    
    @Autowired
    RegionLink_Repository regionLink_Repository;

    @Autowired
    RegionLink_Service regionLink_Service;

    @GetMapping("/all")
    public ResponseEntity<List<RegionLink>> getAllRegion() {
        try {
            return new ResponseEntity<>(regionLink_Service.getListAllRegion(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<RegionLink> createNewAddress(@RequestBody RegionLink pRegion) {
        try {
            RegionLink newRegion = new RegionLink();
            newRegion.setName(pRegion.getName());
            newRegion.setDescription(pRegion.getDescription());
            newRegion.setPhoto(pRegion.getPhoto());
            newRegion.setAddress(pRegion.getAddress());
            newRegion.setLat(pRegion.getLat());
            newRegion.setLng(pRegion.getLng());
            return new ResponseEntity<>(regionLink_Repository.save(newRegion), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<RegionLink> updateAddress(@PathVariable("id") int id, @RequestBody RegionLink pRegion){
        try {
            RegionLink updatedRegion = regionLink_Repository.findById(id);
            if(updatedRegion != null){
                updatedRegion.setName(pRegion.getName());
                updatedRegion.setDescription(pRegion.getDescription());
                updatedRegion.setPhoto(pRegion.getPhoto());
                updatedRegion.setAddress(pRegion.getAddress());
                updatedRegion.setLat(pRegion.getLat());
                updatedRegion.setLng(pRegion.getLng());
                return new ResponseEntity<>(regionLink_Repository.save(updatedRegion),HttpStatus.OK);
            }else{
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<RegionLink> deleteRegion(@PathVariable("id") int id){
        try {
            RegionLink region = regionLink_Repository.findById(id);
            regionLink_Repository.delete(region);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}
