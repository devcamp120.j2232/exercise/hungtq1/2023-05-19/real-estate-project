package com.task83.real_estate_project.API;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import com.task83.real_estate_project.Entities.ConstructionContractor;
import com.task83.real_estate_project.Repository.ConstructionContractor_Repository;
import com.task83.real_estate_project.Service.ConstructionContractor_Service;

@RestController
@CrossOrigin
@RequestMapping("/contractor")
public class ConstructionContractor_Controller {
    
@Autowired
ConstructionContractor_Repository constructionContractor_Repository;

@Autowired
ConstructionContractor_Service constructionContractor_Service;

    @GetMapping("/all")
    public ResponseEntity<List<ConstructionContractor>> getAllContractor(){
        try {
            List<ConstructionContractor> listContractor = new ArrayList<ConstructionContractor>();
            constructionContractor_Repository.findAll().forEach(listContractor::add);
            return new ResponseEntity<>(listContractor, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @PostMapping("/create")
    public ResponseEntity<ConstructionContractor> createNewcontractor(@RequestBody ConstructionContractor pContractor) {
        try {
            ConstructionContractor newContractor = new ConstructionContractor();
            newContractor.setName(pContractor.getName());
            newContractor.setDescription(pContractor.getDescription());
            newContractor.setProjects(pContractor.getProjects());
            newContractor.setAddress(pContractor.getAddress());
            newContractor.setPhone(pContractor.getPhone());
            newContractor.setPhone2(pContractor.getPhone2());
            newContractor.setFax(pContractor.getFax());
            newContractor.setEmail(pContractor.getEmail());
            newContractor.setWebsite(pContractor.getWebsite());
            newContractor.setNote(pContractor.getNote());
            return new ResponseEntity<>(constructionContractor_Repository.save(newContractor), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
    @PutMapping("/update/{id}")
    public ResponseEntity<ConstructionContractor> updateCotnractor(@PathVariable("id") int id, @RequestBody ConstructionContractor pContractor){
        try {
            ConstructionContractor updatedContractor = constructionContractor_Repository.findById(id);
            if(updatedContractor != null){
                updatedContractor.setName(pContractor.getName());
                updatedContractor.setDescription(pContractor.getDescription());
                updatedContractor.setProjects(pContractor.getProjects());
                updatedContractor.setAddress(pContractor.getAddress());
                updatedContractor.setPhone(pContractor.getPhone());
                updatedContractor.setPhone2(pContractor.getPhone2());
                updatedContractor.setFax(pContractor.getFax());
                updatedContractor.setEmail(pContractor.getEmail());
                updatedContractor.setWebsite(pContractor.getWebsite());
                updatedContractor.setNote(pContractor.getNote());
                return new ResponseEntity<>(constructionContractor_Repository.save(updatedContractor),HttpStatus.OK);
            }else{
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ConstructionContractor> deleteContractor(@PathVariable("id") int id){
        try {
            ConstructionContractor contractor = constructionContractor_Repository.findById(id);
            constructionContractor_Repository.delete(contractor);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
