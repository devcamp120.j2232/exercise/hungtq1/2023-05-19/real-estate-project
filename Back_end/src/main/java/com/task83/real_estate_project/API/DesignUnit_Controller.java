package com.task83.real_estate_project.API;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.task83.real_estate_project.Entities.DesignUnit;
import com.task83.real_estate_project.Repository.DesignUnit_Repository;
import com.task83.real_estate_project.Service.DesignUnit_Service;

@RestController
@CrossOrigin
@RequestMapping("/design_unit")
public class DesignUnit_Controller {
    
    @Autowired
    DesignUnit_Repository designUnit_Repository;

    @Autowired
    DesignUnit_Service designUnit_Service;

    @GetMapping("/all")
    public ResponseEntity<List<DesignUnit>> getAllAddress() {
        try {
            return new ResponseEntity<>(designUnit_Service.getListUnit(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<DesignUnit> createNewAddress(@RequestBody DesignUnit pUnit) {
        try {
            DesignUnit newUnit = new DesignUnit();
            newUnit.setName(pUnit.getName());
            newUnit.setDescription(pUnit.getDescription());
            newUnit.setProjects(pUnit.getProjects());
            newUnit.setAddress(pUnit.getAddress());
            newUnit.setPhone(pUnit.getPhone());
            newUnit.setPhone2(pUnit.getPhone2());
            newUnit.setFax(pUnit.getFax());
            newUnit.setEmail(pUnit.getEmail());
            newUnit.setWebsite(pUnit.getWebsite());
            newUnit.setNote(pUnit.getNote());
            return new ResponseEntity<>(designUnit_Repository.save(newUnit), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
    @PutMapping("/update/{id}")
    public ResponseEntity<DesignUnit> updateAddress(@PathVariable("id") int id, @RequestBody DesignUnit pUnit){
        try {
            DesignUnit updatedUnit = designUnit_Repository.findById(id);
            if(updatedUnit != null){
                updatedUnit.setName(pUnit.getName());
                updatedUnit.setDescription(pUnit.getDescription());
                updatedUnit.setProjects(pUnit.getProjects());
                updatedUnit.setAddress(pUnit.getAddress());
                updatedUnit.setPhone(pUnit.getPhone());
                updatedUnit.setPhone2(pUnit.getPhone2());
                updatedUnit.setFax(pUnit.getFax());
                updatedUnit.setEmail(pUnit.getEmail());
                updatedUnit.setWebsite(pUnit.getWebsite());
                updatedUnit.setNote(pUnit.getNote());
                return new ResponseEntity<>(designUnit_Repository.save(updatedUnit),HttpStatus.OK);
            }else{
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<DesignUnit> deleteUnit(@PathVariable("id") int id){
        try {
            DesignUnit unit = designUnit_Repository.findById(id);
            designUnit_Repository.delete(unit);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
