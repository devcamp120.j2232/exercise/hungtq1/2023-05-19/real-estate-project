package com.task83.real_estate_project.API;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.task83.real_estate_project.Entities.Province;
import com.task83.real_estate_project.Repository.Province_Repository;
import com.task83.real_estate_project.Service.Province_Service;

@RestController
@CrossOrigin
@RequestMapping("/province")
public class Province_Controller {
    
    @Autowired
    Province_Repository province_Repository;

    @Autowired
    Province_Service province_Service;

    @GetMapping("/all")
    public ResponseEntity<List<Province>> getAllProvince() {
        try {
            return new ResponseEntity<>(province_Service.getProvinceList(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Province> createNewProvince(@RequestBody Province pProvince) {
        try {
            Province newProvince = new Province();
            newProvince.setName(pProvince.getName());
            newProvince.setCode(pProvince.getCode());
            return new ResponseEntity<>(province_Repository.save(newProvince), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
    @PutMapping("/update/{id}")
    public ResponseEntity<Province> updateAddress(@PathVariable("id") int id, @RequestBody Province pProvince){
        try {
            Province updatedProvince = province_Repository.findById(id);
            if(updatedProvince != null){
                updatedProvince.setName(pProvince.getName());
                updatedProvince.setCode(pProvince.getCode());
                return new ResponseEntity<>(province_Repository.save(updatedProvince),HttpStatus.OK);
            }else{
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Province> deleteProvince(@PathVariable("id") int id){
        try {
            Province province = province_Repository.findById(id);
            province_Repository.delete(province);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
