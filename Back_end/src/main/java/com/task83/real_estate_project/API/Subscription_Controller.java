package com.task83.real_estate_project.API;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.task83.real_estate_project.Entities.Subscription;
import com.task83.real_estate_project.Repository.Subscription_Repository;
import com.task83.real_estate_project.Service.Subscription_Service;

@RestController
@CrossOrigin
@RequestMapping("/subscription")
public class Subscription_Controller {
    
    @Autowired
    Subscription_Repository subscription_Repository;

    @Autowired
    Subscription_Service subscription_Service;


    @GetMapping("/all")
    public ResponseEntity<List<Subscription>> getAllSubscription() {
        try {
            return new ResponseEntity<>(subscription_Service.getListAllSubcription(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Subscription> createNewAddress(@RequestBody Subscription pSubscription) {
        try {
            Subscription newSubscription = new Subscription();
            newSubscription.setUser(pSubscription.getUser());
            newSubscription.setEndpoint(pSubscription.getEndpoint());
            newSubscription.setPublicKey(pSubscription.getPublicKey());
            newSubscription.setAuthenticationToken(pSubscription.getAuthenticationToken());
            newSubscription.setContentCoding(pSubscription.getContentCoding());
            return new ResponseEntity<>(subscription_Repository.save(newSubscription), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
    @PutMapping("/update/{id}")
    public ResponseEntity<Subscription> updateAddress(@PathVariable("id") int id, @RequestBody Subscription pSubscription){
        try {
            Subscription updatedSubscription = subscription_Repository.findById(id);
            if(updatedSubscription != null){
                updatedSubscription.setUser(pSubscription.getUser());
                updatedSubscription.setEndpoint(pSubscription.getEndpoint());
                updatedSubscription.setPublicKey(pSubscription.getPublicKey());
                updatedSubscription.setAuthenticationToken(pSubscription.getAuthenticationToken());
                updatedSubscription.setContentCoding(pSubscription.getContentCoding());
                return new ResponseEntity<>(subscription_Repository.save(updatedSubscription),HttpStatus.OK);
            }else{
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Subscription> deleteAddress(@PathVariable("id") int id){
        try {
            Subscription subscription = subscription_Repository.findById(id);
            subscription_Repository.delete(subscription);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
