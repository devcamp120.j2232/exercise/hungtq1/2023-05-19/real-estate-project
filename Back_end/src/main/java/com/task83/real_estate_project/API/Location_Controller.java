package com.task83.real_estate_project.API;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.task83.real_estate_project.Entities.Location;
import com.task83.real_estate_project.Repository.Location_Repository;
import com.task83.real_estate_project.Service.Location_Service;

@RestController
@CrossOrigin
@RequestMapping("/location")
public class Location_Controller {
    
    @Autowired
    Location_Repository location_Repository;

    @Autowired
    Location_Service location_Service;

    @GetMapping("/all")
    public ResponseEntity<List<Location>> getAllLocation() {
        try {
            return new ResponseEntity<>(location_Service.getListAllLocation(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Location> createNewLocation(@RequestBody Location pLocation) {
        try {
            Location newLocation = new Location();
            newLocation.setLatitude(pLocation.getLatitude());
            newLocation.setLongitude(pLocation.getLongitude());
            return new ResponseEntity<>(location_Repository.save(newLocation), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Location> updateAddress(@PathVariable("id") int id, @RequestBody Location pLocation){
        try {
            Location updatedLocation = location_Repository.findById(id);
            if(updatedLocation != null){
                updatedLocation.setLatitude(pLocation.getLatitude());
                updatedLocation.setLongitude(pLocation.getLongitude());
                return new ResponseEntity<>(location_Repository.save(updatedLocation),HttpStatus.OK);
            }else{
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Location> deleteAddress(@PathVariable("id") int id){
        try {
            Location location = location_Repository.findById(id);
            location_Repository.delete(location);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
