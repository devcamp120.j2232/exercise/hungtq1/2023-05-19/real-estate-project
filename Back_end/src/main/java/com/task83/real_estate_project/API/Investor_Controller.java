package com.task83.real_estate_project.API;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.task83.real_estate_project.Entities.Investor;
import com.task83.real_estate_project.Repository.Investor_Repository;
import com.task83.real_estate_project.Service.Investor_Service;

@RestController
@CrossOrigin
@RequestMapping("/investor")
public class Investor_Controller {
    
    @Autowired
    Investor_Repository investor_Repository;

    @Autowired
    Investor_Service investor_Service;

    @GetMapping("/all")
    public ResponseEntity<List<Investor>> getAllAddress() {
        try {
            return new ResponseEntity<>(investor_Service.getListAllInvestor(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Investor> createNewInvestor(@RequestBody Investor pInvestor) {
        try {
            Investor newInvestor = new Investor();
            newInvestor.setName(pInvestor.getName());
            newInvestor.setDescription(pInvestor.getDescription());
            newInvestor.setProjects(pInvestor.getProjects());
            newInvestor.setAddress(pInvestor.getAddress());
            newInvestor.setPhone(pInvestor.getPhone());
            newInvestor.setPhone2(pInvestor.getPhone2());
            newInvestor.setFax(pInvestor.getFax());
            newInvestor.setEmail(pInvestor.getEmail());
            newInvestor.setWebsite(pInvestor.getWebsite());
            newInvestor.setNote(pInvestor.getNote());
            return new ResponseEntity<>(investor_Repository.save(newInvestor), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @PutMapping("/update/{id}")
    public ResponseEntity<Investor> updateAddress(@PathVariable("id") int id, @RequestBody Investor pInvestor){
        try {
            Investor updatedInvestor = investor_Repository.findById(id);
            if(updatedInvestor != null){
                updatedInvestor.setName(pInvestor.getName());
                updatedInvestor.setDescription(pInvestor.getDescription());
                updatedInvestor.setProjects(pInvestor.getProjects());
                updatedInvestor.setAddress(pInvestor.getAddress());
                updatedInvestor.setPhone(pInvestor.getPhone());
                updatedInvestor.setPhone2(pInvestor.getPhone2());
                updatedInvestor.setFax(pInvestor.getFax());
                updatedInvestor.setEmail(pInvestor.getEmail());
                updatedInvestor.setWebsite(pInvestor.getWebsite());
                updatedInvestor.setNote(pInvestor.getNote());
                return new ResponseEntity<>(investor_Repository.save(updatedInvestor),HttpStatus.OK);
            }else{
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Investor> deleteIvestor(@PathVariable("id") int id){
        try {
            Investor investor = investor_Repository.findById(id);
            investor_Repository.delete(investor);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
