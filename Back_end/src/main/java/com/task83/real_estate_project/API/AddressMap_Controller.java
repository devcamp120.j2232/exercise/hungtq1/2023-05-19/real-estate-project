package com.task83.real_estate_project.API;

import java.util.*;

//import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.task83.real_estate_project.Entities.AddressMap;
import com.task83.real_estate_project.Repository.AddressMap_Repository;
import com.task83.real_estate_project.Service.AddressMap_Service;

@RestController
@CrossOrigin
@RequestMapping("/addressMap")

public class AddressMap_Controller {

    @Autowired
    AddressMap_Repository addressMap_Repository;

    @Autowired
    AddressMap_Service addressMap_Service;

    @GetMapping("/all")
    public ResponseEntity<List<AddressMap>> getAllAddress() {
        try {
            return new ResponseEntity<>(addressMap_Service.getListAllAddress(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<AddressMap> createNewAddress(@RequestBody AddressMap pAddress) {
        try {
            AddressMap newAddress = new AddressMap();
            newAddress.setAddress(pAddress.getAddress());
            newAddress.setLatitude(pAddress.getLatitude());
            newAddress.setLongitude(pAddress.getLongitude());
            return new ResponseEntity<>(addressMap_Repository.save(newAddress), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
    @PutMapping("/update/{id}")
    public ResponseEntity<AddressMap> updateAddress(@PathVariable("id") int id, @RequestBody AddressMap pAddressMap){
        try {
            AddressMap updatedAddress = addressMap_Repository.findById(id);
            if(updatedAddress != null){
                updatedAddress.setAddress(pAddressMap.getAddress());
                updatedAddress.setLatitude(pAddressMap.getLatitude());
                updatedAddress.setLongitude(pAddressMap.getLongitude());
                return new ResponseEntity<>(addressMap_Repository.save(updatedAddress),HttpStatus.OK);
            }else{
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*   @PutMapping("/update/{id}")
        public ResponseEntity<Object> updateAddressMap(@PathVariable("id") Integer id, @Valid @RequestBody AddressMap paramAddressMap) {
            Optional<AddressMap> vAddressMapData = addressMap_Repository.findById(id);
            if (vAddressMapData.isPresent()) {
                try {
                    return new ResponseEntity<>(addressMap_Service.updateAddressMap(paramAddressMap, vAddressMapData), HttpStatus.OK);
                } catch (Exception e) {
                    return ResponseEntity.unprocessableEntity()
                            .body("Failed to Update specified AddressMap: " + e.getCause().getCause().getMessage());
                }
            } else {
                AddressMap vAddressMapNull = new AddressMap();
                return new ResponseEntity<>(vAddressMapNull, HttpStatus.NOT_FOUND);
            }
        }
    */

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<AddressMap> deleteAddress(@PathVariable("id") int id){
        try {
            AddressMap address = addressMap_Repository.findById(id);
            addressMap_Repository.delete(address);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
