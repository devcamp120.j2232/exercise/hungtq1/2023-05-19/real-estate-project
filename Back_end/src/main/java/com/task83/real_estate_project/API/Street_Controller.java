package com.task83.real_estate_project.API;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.task83.real_estate_project.Entities.District;
import com.task83.real_estate_project.Entities.Street;
import com.task83.real_estate_project.Repository.District_Repository;
import com.task83.real_estate_project.Repository.Street_Repository;
import com.task83.real_estate_project.Service.Street_Service;

@RestController
@CrossOrigin
@RequestMapping("/street")
public class Street_Controller {
    
    @Autowired
    Street_Repository street_Repository;

    @Autowired
    Street_Service street_Service;

    @Autowired
    District_Repository district_Repository;

    @GetMapping("/all")
    public ResponseEntity<List<Street>> getAllStreet() {
        try {
            return new ResponseEntity<>(street_Service.getListAllStreet(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping("/create/{districtId}")
    public ResponseEntity<Object> createWard(@PathVariable(value = "districtId") int districtId,  @RequestBody Street pStreet){
        try {
            District district = district_Repository.findById(districtId);
            if(district != null){
                Street newStreet = new Street();
                newStreet.setName(pStreet.getName());
                newStreet.setPrefix(pStreet.getPrefix());
                newStreet.setDistrict(district);
                return new ResponseEntity<>(street_Repository.save(newStreet), HttpStatus.CREATED);
            }
            else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{streetId}/{districtId}")
    public ResponseEntity<Object> streetUpdate(@PathVariable("streetId") int streetId, @PathVariable("districtId") int districtId, @RequestBody Street pStreet ){
        try {
            Street street = street_Repository.findById(streetId);
            District district = district_Repository.findById(districtId);
            if(street != null){
                street.setName(pStreet.getName());
                street.setPrefix(pStreet.getPrefix());
                street.setDistrict(district);
                return new ResponseEntity<>(street_Repository.save(street), HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Street> deleteStreet(@PathVariable("id") int id){
        try {
            Street street = street_Repository.findById(id);
            street_Repository.delete(street);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);          
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
