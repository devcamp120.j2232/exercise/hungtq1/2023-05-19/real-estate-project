package com.task83.real_estate_project.API;

import java.util.*;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.task83.real_estate_project.Entities.Customer;
import com.task83.real_estate_project.Repository.Customer_Repository;
import com.task83.real_estate_project.Service.Customer_Service;

@RestController
@CrossOrigin
@RequestMapping("/customer")
public class Customer_Controller {
    
    @Autowired
    Customer_Repository customer_Repository;

    @Autowired
    Customer_Service customer_Service;

    @GetMapping("/all")
    public ResponseEntity<List<Customer>> getAllCustomer() {
        try {
            return new ResponseEntity<>(customer_Service.getListCustomer(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Customer> createNewCustomer(@RequestBody Customer pCustomer) {
        try {
            Customer newCustomer = new Customer();
            newCustomer.setContact_name(pCustomer.getContact_name());
            newCustomer.setContact_title(pCustomer.getContact_title());
            newCustomer.setAddress(pCustomer.getAddress());
            newCustomer.setMobile(pCustomer.getMobile());
            newCustomer.setEmail(pCustomer.getEmail());
            newCustomer.setNote(pCustomer.getNote());
            newCustomer.setCreate_by(pCustomer.getCreate_by());
            newCustomer.setUpdate_by(pCustomer.getUpdate_by());
            newCustomer.setCreate_date(new Date(0));;
            newCustomer.setUpdate_date(null);;
            return new ResponseEntity<>(customer_Repository.save(newCustomer), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
    @PutMapping("/update/{id}")
    public ResponseEntity<Customer> updateAddress(@PathVariable("id") int id, @RequestBody Customer pCustomer){
        try {
            Customer updatedCustomer = customer_Repository.findById(id);
            if(updatedCustomer != null){
                updatedCustomer.setContact_name(pCustomer.getContact_name());
                updatedCustomer.setContact_title(pCustomer.getContact_title());
                updatedCustomer.setAddress(pCustomer.getAddress());
                updatedCustomer.setMobile(pCustomer.getMobile());
                updatedCustomer.setEmail(pCustomer.getEmail());
                updatedCustomer.setNote(pCustomer.getNote());
                updatedCustomer.setCreate_by(pCustomer.getCreate_by());
                updatedCustomer.setUpdate_by(pCustomer.getUpdate_by());
                //updatedCustomer.setCreate_date(null);
                updatedCustomer.setUpdate_date(new Date(0));
                return new ResponseEntity<>(customer_Repository.save(updatedCustomer),HttpStatus.OK);
            }else{
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Customer> deleteCustomer(@PathVariable("id") int id){
        try {
            Customer customer = customer_Repository.findById(id);
            customer_Repository.delete(customer);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
