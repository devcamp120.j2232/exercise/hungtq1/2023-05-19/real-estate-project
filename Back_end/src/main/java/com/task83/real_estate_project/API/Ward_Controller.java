package com.task83.real_estate_project.API;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.task83.real_estate_project.Entities.District;
import com.task83.real_estate_project.Entities.Ward;
import com.task83.real_estate_project.Repository.District_Repository;
import com.task83.real_estate_project.Repository.Ward_Repository;
import com.task83.real_estate_project.Service.Ward_Service;

@RestController
@CrossOrigin
@RequestMapping("/ward")
public class Ward_Controller {
    
    @Autowired
    Ward_Repository ward_Repository;

    @Autowired
    Ward_Service ward_Service;

    @Autowired
    District_Repository district_Repository;

    @GetMapping("/all")
    public ResponseEntity<List<Ward>> getAllWard() {
        try {
            return new ResponseEntity<>(ward_Service.getListAllWard(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping("/create/{districtId}")
    public ResponseEntity<Object> createWard(@PathVariable(value = "districtId") int districtId,  @RequestBody Ward pWard){
        try {
            District district = district_Repository.findById(districtId);
            if(district != null){
                Ward newWard = new Ward();
                newWard.setName(pWard.getName());
                newWard.setPrefix(pWard.getPrefix());
                newWard.setDistrict(district);
                return new ResponseEntity<>(ward_Repository.save(newWard), HttpStatus.CREATED);
            }
            else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{wardId}/{districtId}")
    public ResponseEntity<Object> wardUpdate(@PathVariable("wardId") int wardId, @PathVariable("districtId") int districtId, @RequestBody Ward pWard ){
        try {
            Ward ward = ward_Repository.findById(wardId);
            District district = district_Repository.findById(districtId);
            if(ward != null){
                ward.setName(pWard.getName());
                ward.setPrefix(pWard.getPrefix());
                ward.setDistrict(district);
                return new ResponseEntity<>(ward_Repository.save(ward), HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Ward> deleteWard(@PathVariable("id") int id){
        try {
            Ward ward = ward_Repository.findById(id);
            ward_Repository.delete(ward);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);          
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
