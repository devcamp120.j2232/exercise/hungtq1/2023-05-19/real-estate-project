package com.task83.real_estate_project.API;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.task83.real_estate_project.Entities.Employee;
import com.task83.real_estate_project.Repository.Employee_Repository;
import com.task83.real_estate_project.Service.Employee_Service;

@RestController
@CrossOrigin
@RequestMapping("/employee")
public class Employee_Controller {
    
    @Autowired
    Employee_Repository employee_Repository;

    @Autowired
    Employee_Service employee_Service;

    @GetMapping("/all")
    public ResponseEntity<List<Employee>> getAllRegion() {
        try {
            return new ResponseEntity<>(employee_Service.getListEmployee(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Employee> createNewEmployee(@RequestBody Employee pEmployee) {
        try {
            Employee newEmployee = new Employee();
            newEmployee.setLastName(pEmployee.getLastName());
            newEmployee.setFirstName(pEmployee.getFirstName());
            newEmployee.setTitle(pEmployee.getTitle());
            newEmployee.setTitleOfCourtesy(pEmployee.getTitleOfCourtesy());
            newEmployee.setBirthDate(pEmployee.getBirthDate());
            newEmployee.setHireDate(pEmployee.getHireDate());
            newEmployee.setAddress(pEmployee.getAddress());
            newEmployee.setCity(pEmployee.getCity());
            newEmployee.setRegion(pEmployee.getRegion());
            newEmployee.setPostalCode(pEmployee.getPostalCode());
            newEmployee.setCountry(pEmployee.getCountry());
            newEmployee.setHomePhone(pEmployee.getHomePhone());
            newEmployee.setExtension(pEmployee.getExtension());
            newEmployee.setPhoto(pEmployee.getPhoto());
            newEmployee.setNotes(pEmployee.getNotes());
            newEmployee.setReportsTo(pEmployee.getReportsTo());
            newEmployee.setUsername(pEmployee.getUsername());
            newEmployee.setPassword(pEmployee.getPassword());
            newEmployee.setEmail(pEmployee.getEmail());
            newEmployee.setActivated(pEmployee.getActivated());
            newEmployee.setProfile(pEmployee.getProfile());
            newEmployee.setUserLevel(pEmployee.getUserLevel());
            return new ResponseEntity<>(employee_Repository.save(newEmployee), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable("id") int id, @RequestBody Employee pEmployee){
        try {
            Employee updatedEmployee = employee_Repository.findById(id);
            if(updatedEmployee != null){
                updatedEmployee.setLastName(pEmployee.getLastName());
                updatedEmployee.setFirstName(pEmployee.getFirstName());
                updatedEmployee.setTitle(pEmployee.getTitle());
                updatedEmployee.setTitleOfCourtesy(pEmployee.getTitleOfCourtesy());
                updatedEmployee.setBirthDate(pEmployee.getBirthDate());
                updatedEmployee.setHireDate(pEmployee.getHireDate());
                updatedEmployee.setAddress(pEmployee.getAddress());
                updatedEmployee.setCity(pEmployee.getCity());
                updatedEmployee.setRegion(pEmployee.getRegion());
                updatedEmployee.setPostalCode(pEmployee.getPostalCode());
                updatedEmployee.setCountry(pEmployee.getCountry());
                updatedEmployee.setHomePhone(pEmployee.getHomePhone());
                updatedEmployee.setExtension(pEmployee.getExtension());
                updatedEmployee.setPhoto(pEmployee.getPhoto());
                updatedEmployee.setNotes(pEmployee.getNotes());
                updatedEmployee.setReportsTo(pEmployee.getReportsTo());
                updatedEmployee.setUsername(pEmployee.getUsername());
                updatedEmployee.setPassword(pEmployee.getPassword());
                updatedEmployee.setEmail(pEmployee.getEmail());
                updatedEmployee.setActivated(pEmployee.getActivated());
                updatedEmployee.setProfile(pEmployee.getProfile());
                updatedEmployee.setUserLevel(pEmployee.getUserLevel());
                return new ResponseEntity<>(employee_Repository.save(updatedEmployee),HttpStatus.OK);
            }else{
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Employee> deleteEmployee(@PathVariable("id") int id){
        try {
            Employee employee = employee_Repository.findById(id);
            employee_Repository.delete(employee);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
