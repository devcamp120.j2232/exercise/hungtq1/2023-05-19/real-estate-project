package com.task83.real_estate_project.API;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.task83.real_estate_project.Entities.Utilities;
import com.task83.real_estate_project.Repository.Utilities_Repository;
import com.task83.real_estate_project.Service.Utilities_Service;

@RestController
@CrossOrigin
@RequestMapping("/utilities")
public class Utilities_Controller {
    
    @Autowired
    Utilities_Repository utilities_Repository;

    @Autowired
    Utilities_Service utilities_Service;

    @GetMapping("/all")
    public ResponseEntity<List<Utilities>> getAllUtilities() {
        try {
            return new ResponseEntity<>(utilities_Service.getListAllUtilities(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Utilities> createNewAddress(@RequestBody Utilities pUtility) {
        try {
            Utilities newUtility = new Utilities();
            newUtility.setName(pUtility.getName());
            newUtility.setDescription(pUtility.getDescription());
            newUtility.setPhoto(pUtility.getPhoto());
            return new ResponseEntity<>(utilities_Repository.save(newUtility), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
    @PutMapping("/update/{id}")
    public ResponseEntity<Utilities> updateAddress(@PathVariable("id") int id, @RequestBody Utilities pUtility){
        try {
            Utilities updatedUtility = utilities_Repository.findById(id);
            if(updatedUtility != null){
                updatedUtility.setName(pUtility.getName());
                updatedUtility.setDescription(pUtility.getDescription());
                updatedUtility.setPhoto(pUtility.getPhoto());
                return new ResponseEntity<>(utilities_Repository.save(updatedUtility),HttpStatus.OK);
            }else{
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Utilities> deleteUtility(@PathVariable("id") int id){
        try {
            Utilities utility = utilities_Repository.findById(id);
            utilities_Repository.delete(utility);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
