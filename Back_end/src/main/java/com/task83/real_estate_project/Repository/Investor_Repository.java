package com.task83.real_estate_project.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task83.real_estate_project.Entities.Investor;

public interface Investor_Repository extends JpaRepository<Investor, Integer>{
    Investor findById(int id);
}
