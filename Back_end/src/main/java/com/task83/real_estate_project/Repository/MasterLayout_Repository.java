package com.task83.real_estate_project.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task83.real_estate_project.Entities.MasterLayout;

public interface MasterLayout_Repository extends JpaRepository<MasterLayout, Integer>{
    MasterLayout findById(int id);
}
