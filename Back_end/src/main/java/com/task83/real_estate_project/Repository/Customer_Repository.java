package com.task83.real_estate_project.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task83.real_estate_project.Entities.Customer;

public interface Customer_Repository extends JpaRepository<Customer, Integer>{
    Customer findById(int id);
}
