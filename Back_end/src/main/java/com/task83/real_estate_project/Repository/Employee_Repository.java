package com.task83.real_estate_project.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task83.real_estate_project.Entities.Employee;

public interface Employee_Repository extends JpaRepository<Employee, Integer> {
    Employee findById(int id);
}
