package com.task83.real_estate_project.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task83.real_estate_project.Entities.AddressMap;

public interface AddressMap_Repository extends JpaRepository<AddressMap, Integer>{
    AddressMap findById(int id);
    
}
