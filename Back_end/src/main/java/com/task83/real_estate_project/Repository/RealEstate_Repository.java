package com.task83.real_estate_project.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task83.real_estate_project.Entities.RealEstate;

public interface RealEstate_Repository extends JpaRepository<RealEstate, Integer>{
    RealEstate findById(int id);
}
