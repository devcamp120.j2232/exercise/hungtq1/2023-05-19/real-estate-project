package com.task83.real_estate_project.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task83.real_estate_project.Entities.Utilities;

public interface Utilities_Repository extends JpaRepository<Utilities, Integer>{
    Utilities findById(int id);
}
