package com.task83.real_estate_project.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task83.real_estate_project.Entities.ConstructionContractor;

public interface ConstructionContractor_Repository extends JpaRepository<ConstructionContractor, Integer>{
    ConstructionContractor findById(int id);
}
