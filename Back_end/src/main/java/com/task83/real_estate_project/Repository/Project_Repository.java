package com.task83.real_estate_project.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task83.real_estate_project.Entities.Project;

public interface Project_Repository extends JpaRepository<Project, Integer>{
    Project findById(int id);
}
