package com.task83.real_estate_project.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task83.real_estate_project.Entities.Location;

public interface Location_Repository extends JpaRepository<Location, Integer>{
    Location findById(int id);
}
