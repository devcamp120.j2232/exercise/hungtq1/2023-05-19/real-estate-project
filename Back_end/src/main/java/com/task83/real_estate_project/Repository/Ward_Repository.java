package com.task83.real_estate_project.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task83.real_estate_project.Entities.Ward;

public interface Ward_Repository extends JpaRepository<Ward, Integer>{
    Ward findById(int id);
}
