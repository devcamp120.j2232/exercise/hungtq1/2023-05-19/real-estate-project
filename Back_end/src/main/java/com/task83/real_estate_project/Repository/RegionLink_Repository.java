package com.task83.real_estate_project.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task83.real_estate_project.Entities.RegionLink;

public interface RegionLink_Repository extends JpaRepository<RegionLink, Integer>{
    RegionLink findById(int id);
}
