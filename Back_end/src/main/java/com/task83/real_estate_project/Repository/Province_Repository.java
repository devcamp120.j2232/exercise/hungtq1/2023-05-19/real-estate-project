package com.task83.real_estate_project.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task83.real_estate_project.Entities.Province;

public interface Province_Repository extends JpaRepository<Province, Integer> {
    Province findById(int id);
}
