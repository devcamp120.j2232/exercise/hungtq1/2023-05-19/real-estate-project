package com.task83.real_estate_project.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task83.real_estate_project.Entities.DesignUnit;

public interface DesignUnit_Repository extends JpaRepository<DesignUnit, Integer>{
    DesignUnit findById(int id);
}
