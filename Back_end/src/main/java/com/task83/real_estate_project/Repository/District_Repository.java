package com.task83.real_estate_project.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task83.real_estate_project.Entities.District;

public interface District_Repository extends JpaRepository<District, Integer>{
    District findById(int id);
}
