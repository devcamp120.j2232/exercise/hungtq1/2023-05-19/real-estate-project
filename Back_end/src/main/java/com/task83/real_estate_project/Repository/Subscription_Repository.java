package com.task83.real_estate_project.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task83.real_estate_project.Entities.Subscription;

public interface Subscription_Repository extends JpaRepository<Subscription, Integer>{
    Subscription findById(int id);
}
