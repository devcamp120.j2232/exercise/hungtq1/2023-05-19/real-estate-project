package com.task83.real_estate_project.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task83.real_estate_project.Entities.RegionLink;
import com.task83.real_estate_project.Repository.RegionLink_Repository;

@Service
public class RegionLink_Service {
    
    @Autowired
    RegionLink_Repository regionLink_Repository;

    public ArrayList<RegionLink> getListAllRegion(){
        ArrayList<RegionLink> listRegion = new ArrayList<>();
        regionLink_Repository.findAll().forEach(listRegion::add);
        return listRegion;
    }
}
