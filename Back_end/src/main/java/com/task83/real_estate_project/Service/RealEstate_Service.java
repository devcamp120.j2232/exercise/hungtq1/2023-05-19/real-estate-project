package com.task83.real_estate_project.Service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task83.real_estate_project.Entities.*;

import com.task83.real_estate_project.Repository.RealEstate_Repository;

@Service
public class RealEstate_Service {
    
    @Autowired
    RealEstate_Repository realEstate_Repository;

    public ArrayList<RealEstate> getListAllRealEstate(){
        ArrayList<RealEstate> listRealEstates = new ArrayList<>();
        realEstate_Repository.findAll().forEach(listRealEstates::add);
        return listRealEstates;
    }

    public RealEstate createRealeState(RealEstate pRealeState,
    Optional<Street> pStreetData, Optional<Ward> pWardData, Optional<District> pDistrictData,
    Optional<Province> pProvinceData, Optional<Project> pProjectData,
    Optional<Customer> pCustomerData) {
  try {
    RealEstate vRealeState = new RealEstate();
    vRealeState.setAcreage(pRealeState.getAcreage());
    vRealeState.setAddress(pRealeState.getAddress());
    vRealeState.setAdjacentAlleyMinWidth(pRealeState.getAdjacentAlleyMinWidth());
    vRealeState.setAdjacentFacadeNum(pRealeState.getAdjacentFacadeNum());
    vRealeState.setAdjacentRoad(pRealeState.getAdjacentRoad());
    vRealeState.setDescription(pRealeState.getDescription());
    vRealeState.setApartCode(pRealeState.getApartCode());
    vRealeState.setApartType(pRealeState.getApartType());
    vRealeState.setLat(pRealeState.getLat());
    vRealeState.setLng(pRealeState.getLng());
    vRealeState.setApartLoca(pRealeState.getApartLoca());
    vRealeState.setBalcony(pRealeState.getBalcony());
    vRealeState.setBath(pRealeState.getBath());
    vRealeState.setBedroom(pRealeState.getBedroom());
    vRealeState.setCLCL(pRealeState.getCLCL());
    vRealeState.setCTXDPrice(pRealeState.getCTXDPrice());
    vRealeState.setCTXDValue(pRealeState.getCTXDValue());
    vRealeState.setCreateBy(pRealeState.getCreateBy());
    vRealeState.setDTSXD(pRealeState.getDTSXD());
    vRealeState.setDateCreate(pRealeState.getDateCreate());
    vRealeState.setDirection(pRealeState.getDirection());
    vRealeState.setDistance2facade(pRealeState.getDistance2facade());
    vRealeState.setFactor(pRealeState.getFactor());
    vRealeState.setFsbo(pRealeState.getFsbo());
    vRealeState.setFurnitureType(pRealeState.getFurnitureType());
    vRealeState.setLandscapeView(pRealeState.getLandscapeView());
    vRealeState.setLegalDoc(pRealeState.getLegalDoc());
    vRealeState.setLongX(pRealeState.getLongX());
    vRealeState.setNumberFloors(pRealeState.getNumberFloors());
    vRealeState.setPhoto(pRealeState.getPhoto());
    vRealeState.setPrice(pRealeState.getPrice());
    vRealeState.setPriceMin(pRealeState.getPriceMin());
    vRealeState.setPriceRent(pRealeState.getPriceRent());
    vRealeState.setPriceTime(pRealeState.getPriceTime());
    vRealeState.setRequest(pRealeState.getRequest());
    vRealeState.setReturnRate(pRealeState.getReturnRate());
    vRealeState.setShape(pRealeState.getShape());
    vRealeState.setStreetHouse(pRealeState.getStreetHouse());
    vRealeState.setStructure(pRealeState.getStructure());
    vRealeState.setTitle(pRealeState.getTitle());
    vRealeState.setTotalFloors(pRealeState.getTotalFloors());
    vRealeState.setType(pRealeState.getType());
    vRealeState.setUpdateBy(pRealeState.getUpdateBy());
    vRealeState.setViewNum(pRealeState.getViewNum());
    vRealeState.setWallArea(pRealeState.getWallArea());
    vRealeState.setWidthY(pRealeState.getWidthY());

    vRealeState.setStreet(pStreetData.get());
    vRealeState.setWard(pWardData.get());
    vRealeState.setProvince(pProvinceData.get());
    vRealeState.setDistrict(pDistrictData.get());
    vRealeState.setProject(pProjectData.get());
    vRealeState.setCustomer(pCustomerData.get());
    RealEstate vRealeStateSave = realEstate_Repository.save(vRealeState);
    return vRealeStateSave;
  } catch (Exception e) {
    return null;
  }
}

public RealEstate updateRealeState(RealEstate pRealeState, Optional<RealEstate> pRealeStateData,
    Optional<Street> pStreetData,
    Optional<Ward> pWardData,
    Optional<District> pDistrictData, Optional<Province> pProvinceData, Optional<Project> pProjectData,
    Optional<Customer> pCustomerData) {
  try {
    RealEstate vRealeState = new RealEstate();
    vRealeState.setAcreage(pRealeState.getAcreage());
    vRealeState.setAddress(pRealeState.getAddress());
    vRealeState.setAdjacentAlleyMinWidth(pRealeState.getAdjacentAlleyMinWidth());
    vRealeState.setAdjacentFacadeNum(pRealeState.getAdjacentFacadeNum());
    vRealeState.setAdjacentRoad(pRealeState.getAdjacentRoad());
    vRealeState.setDescription(pRealeState.getDescription());
    vRealeState.setApartCode(pRealeState.getApartCode());
    vRealeState.setApartType(pRealeState.getApartType());
    vRealeState.setLat(pRealeState.getLat());
    vRealeState.setLng(pRealeState.getLng());
    vRealeState.setApartLoca(pRealeState.getApartLoca());
    vRealeState.setBalcony(pRealeState.getBalcony());
    vRealeState.setBath(pRealeState.getBath());
    vRealeState.setBedroom(pRealeState.getBedroom());
    vRealeState.setCLCL(pRealeState.getCLCL());
    vRealeState.setCTXDPrice(pRealeState.getCTXDPrice());
    vRealeState.setCTXDValue(pRealeState.getCTXDValue());
    vRealeState.setCreateBy(pRealeState.getCreateBy());
    vRealeState.setDTSXD(pRealeState.getDTSXD());
    vRealeState.setDateCreate(pRealeState.getDateCreate());
    vRealeState.setDirection(pRealeState.getDirection());
    vRealeState.setDistance2facade(pRealeState.getDistance2facade());
    vRealeState.setFactor(pRealeState.getFactor());
    vRealeState.setFsbo(pRealeState.getFsbo());
    vRealeState.setFurnitureType(pRealeState.getFurnitureType());
    vRealeState.setLandscapeView(pRealeState.getLandscapeView());
    vRealeState.setLegalDoc(pRealeState.getLegalDoc());
    vRealeState.setLongX(pRealeState.getLongX());
    vRealeState.setNumberFloors(pRealeState.getNumberFloors());
    vRealeState.setPhoto(pRealeState.getPhoto());
    vRealeState.setPrice(pRealeState.getPrice());
    vRealeState.setPriceMin(pRealeState.getPriceMin());
    vRealeState.setPriceRent(pRealeState.getPriceRent());
    vRealeState.setPriceTime(pRealeState.getPriceTime());
    vRealeState.setRequest(pRealeState.getRequest());
    vRealeState.setReturnRate(pRealeState.getReturnRate());
    vRealeState.setShape(pRealeState.getShape());
    vRealeState.setStreetHouse(pRealeState.getStreetHouse());
    vRealeState.setStructure(pRealeState.getStructure());
    vRealeState.setTitle(pRealeState.getTitle());
    vRealeState.setTotalFloors(pRealeState.getTotalFloors());
    vRealeState.setType(pRealeState.getType());
    vRealeState.setUpdateBy(pRealeState.getUpdateBy());
    vRealeState.setViewNum(pRealeState.getViewNum());
    vRealeState.setWallArea(pRealeState.getWallArea());
    vRealeState.setWidthY(pRealeState.getWidthY());

    vRealeState.setStreet(pStreetData.get());
    vRealeState.setWard(pWardData.get());
    vRealeState.setProvince(pProvinceData.get());
    vRealeState.setDistrict(pDistrictData.get());
    vRealeState.setProject(pProjectData.get());
    vRealeState.setCustomer(pCustomerData.get());
    RealEstate vRealeStateSave = realEstate_Repository.save(vRealeState);
    return vRealeStateSave;
  } catch (Exception e) {
    return null;
  }
}







}
