package com.task83.real_estate_project.Service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task83.real_estate_project.Entities.MasterLayout;
import com.task83.real_estate_project.Repository.MasterLayout_Repository;

@Service
public class MasterLayout_Service {
    
    @Autowired
    MasterLayout_Repository masterLayout_Repository;

    public ArrayList<MasterLayout> getListAllMasterLayout(){
        ArrayList<MasterLayout> listMasterLayout = new ArrayList<>();
        masterLayout_Repository.findAll().forEach(listMasterLayout::add);
        return listMasterLayout;
    }

}
