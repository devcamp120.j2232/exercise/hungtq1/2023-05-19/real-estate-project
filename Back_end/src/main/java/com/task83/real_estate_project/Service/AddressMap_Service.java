package com.task83.real_estate_project.Service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task83.real_estate_project.Entities.AddressMap;
import com.task83.real_estate_project.Repository.AddressMap_Repository;

@Service
public class AddressMap_Service {
    
    @Autowired
    AddressMap_Repository addressMap_Repository;

    public ArrayList<AddressMap> getListAllAddress(){
        ArrayList<AddressMap> listAddress = new ArrayList<>();
        addressMap_Repository.findAll().forEach(listAddress::add);
        return listAddress;
    }

    public AddressMap updateAddressMap(AddressMap pAddressMap, Optional<AddressMap> pAddressMapData) {
        try {
          AddressMap vAddressMap = pAddressMapData.get();
          vAddressMap.setAddress(pAddressMap.getAddress());
          vAddressMap.setLatitude(pAddressMap.getLatitude());
          vAddressMap.setLongitude(pAddressMap.getLongitude());
          AddressMap vAddressMapSave = addressMap_Repository.save(vAddressMap);
          return vAddressMapSave;
        } catch (Exception e) {
          return null;
        }
      }
}
