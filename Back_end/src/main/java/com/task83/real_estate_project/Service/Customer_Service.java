package com.task83.real_estate_project.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task83.real_estate_project.Entities.Customer;
import com.task83.real_estate_project.Repository.Customer_Repository;

@Service
public class Customer_Service {
    
    @Autowired
    Customer_Repository customer_Repository;

    public ArrayList<Customer> getListCustomer(){
        ArrayList<Customer> listCustomer = new ArrayList<>();
        customer_Repository.findAll().forEach(listCustomer::add);
        return listCustomer;
    }
}
