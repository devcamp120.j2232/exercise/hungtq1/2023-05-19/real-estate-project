package com.task83.real_estate_project.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task83.real_estate_project.Entities.ConstructionContractor;
import com.task83.real_estate_project.Repository.ConstructionContractor_Repository;

@Service
public class ConstructionContractor_Service {
    
    @Autowired
    ConstructionContractor_Repository constructionContractor_Repository;

    public ArrayList<ConstructionContractor> getListAllContractor(){
        ArrayList<ConstructionContractor> listContractor = new ArrayList<>();
        constructionContractor_Repository.findAll().forEach(listContractor::add);
        return listContractor;
    }
}
