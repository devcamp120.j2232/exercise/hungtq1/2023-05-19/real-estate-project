package com.task83.real_estate_project.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task83.real_estate_project.Entities.Location;
import com.task83.real_estate_project.Repository.Location_Repository;

@Service
public class Location_Service {
    
    @Autowired
    Location_Repository location_Repository;

    public ArrayList<Location> getListAllLocation(){
        ArrayList<Location> listLocation = new ArrayList<>();
        location_Repository.findAll().forEach(listLocation::add);
        return listLocation;
    }
}
