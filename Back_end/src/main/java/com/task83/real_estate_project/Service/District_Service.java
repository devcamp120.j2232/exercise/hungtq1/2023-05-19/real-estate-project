package com.task83.real_estate_project.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task83.real_estate_project.Entities.District;
import com.task83.real_estate_project.Repository.District_Repository;

@Service
public class District_Service {
    
    @Autowired
    District_Repository district_Repository;

    public ArrayList<District> getListDistrict(){
        ArrayList<District> list = new ArrayList<>();
        district_Repository.findAll().forEach(list::add);
        return list;
    }
}
