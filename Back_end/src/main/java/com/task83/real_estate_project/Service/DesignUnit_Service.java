package com.task83.real_estate_project.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task83.real_estate_project.Entities.DesignUnit;
import com.task83.real_estate_project.Repository.DesignUnit_Repository;

@Service
public class DesignUnit_Service {
    
    @Autowired
    DesignUnit_Repository designUnit_Repository;

    public ArrayList<DesignUnit> getListUnit(){
        ArrayList<DesignUnit> listUnit = new ArrayList<>();
        designUnit_Repository.findAll().forEach(listUnit::add);
        return listUnit;
    }
}
