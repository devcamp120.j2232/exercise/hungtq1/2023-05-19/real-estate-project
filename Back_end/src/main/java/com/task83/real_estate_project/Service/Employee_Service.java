package com.task83.real_estate_project.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task83.real_estate_project.Entities.Employee;
import com.task83.real_estate_project.Repository.Employee_Repository;

@Service
public class Employee_Service {
    
    @Autowired
    Employee_Repository employee_Repository;

    public ArrayList<Employee> getListEmployee(){
        ArrayList<Employee> list = new ArrayList<>();
        employee_Repository.findAll().forEach(list::add);
        return list;
    }
}
