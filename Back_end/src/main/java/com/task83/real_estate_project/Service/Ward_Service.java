package com.task83.real_estate_project.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task83.real_estate_project.Entities.Ward;
import com.task83.real_estate_project.Repository.Ward_Repository;

@Service
public class Ward_Service {
    
    @Autowired
    Ward_Repository ward_Repository;

    public ArrayList<Ward> getListAllWard(){
        ArrayList<Ward> listWard = new ArrayList<>();
        ward_Repository.findAll().forEach(listWard::add);
        return listWard;
    }

}
