package com.task83.real_estate_project.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task83.real_estate_project.Entities.Utilities;
import com.task83.real_estate_project.Repository.Utilities_Repository;

@Service
public class Utilities_Service {
    
    @Autowired
    Utilities_Repository utilities_Repository;

    public ArrayList<Utilities> getListAllUtilities(){
        ArrayList<Utilities> listUtilities = new ArrayList<>();
        utilities_Repository.findAll().forEach(listUtilities::add);
        return listUtilities;
    }

}
