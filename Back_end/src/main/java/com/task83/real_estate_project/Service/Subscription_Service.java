package com.task83.real_estate_project.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task83.real_estate_project.Entities.Subscription;
import com.task83.real_estate_project.Repository.Subscription_Repository;

@Service
public class Subscription_Service {
    
    @Autowired
    Subscription_Repository subscription_Repository;

    public ArrayList<Subscription> getListAllSubcription(){
        ArrayList<Subscription> listSubscription = new ArrayList<>();
        subscription_Repository.findAll().forEach(listSubscription::add);
        return listSubscription;
    }
}
