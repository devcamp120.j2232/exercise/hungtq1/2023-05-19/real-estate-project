package com.task83.real_estate_project.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task83.real_estate_project.Entities.Province;
import com.task83.real_estate_project.Repository.Province_Repository;

@Service
public class Province_Service {
    
    @Autowired
    Province_Repository province_Repository;

    public ArrayList<Province> getProvinceList(){
        ArrayList<Province> listProvince = new ArrayList<>();
        province_Repository.findAll().forEach(listProvince::add);
        return listProvince;
    }
}
