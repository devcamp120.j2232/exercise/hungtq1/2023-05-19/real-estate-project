package com.task83.real_estate_project.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task83.real_estate_project.Entities.Street;
import com.task83.real_estate_project.Repository.Street_Repository;

@Service
public class Street_Service {
    
    @Autowired
    Street_Repository street_Repository;

    public ArrayList<Street> getListAllStreet(){
        ArrayList<Street> listStreet = new ArrayList<>();
        street_Repository.findAll().forEach(listStreet::add);
        return listStreet;
    }
}
