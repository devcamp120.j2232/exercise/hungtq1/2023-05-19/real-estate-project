package com.task83.real_estate_project.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task83.real_estate_project.Entities.Project;
import com.task83.real_estate_project.Repository.Project_Repository;

@Service
public class Project_Service {
    
    @Autowired
    Project_Repository project_Repository;
   
    public ArrayList<Project> getListAllProjects(){
        ArrayList<Project> listProject = new ArrayList<>();
        project_Repository.findAll().forEach(listProject::add);
        return listProject;
    }
}
