package com.task83.real_estate_project.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task83.real_estate_project.Entities.Investor;
import com.task83.real_estate_project.Repository.Investor_Repository;

@Service
public class Investor_Service {
    
    @Autowired
    Investor_Repository investor_Repository;

    public ArrayList<Investor> getListAllInvestor(){
        ArrayList<Investor> listInvestor = new ArrayList<>();
        investor_Repository.findAll().forEach(listInvestor::add);
        return listInvestor;
    }

}
